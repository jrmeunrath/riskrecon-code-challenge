import React from "react"

import { MessagesContextProvider } from "../../contexts/messages.context"
import MessageNotification from "./MessageNotification"

function MessageNotificationWidget({ isApiStarted, messages, onStartFeed }) {
  return (
    <MessagesContextProvider messages={messages}>
      <MessageNotification
        isApiStarted={isApiStarted}
        onStartFeed={onStartFeed}
      />
    </MessagesContextProvider>
  )
}

export default MessageNotificationWidget
