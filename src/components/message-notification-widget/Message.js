import React from "react"
import { useMessagesContext } from "../../contexts/messages.context"
import { MESSAGE_TYPES } from "./constants"

import styles from "./Message.module.scss"

function Message({ messageType }) {
  const {
    errorMessages,
    infoMessages,
    onClearMessage,
    warningMessages,
  } = useMessagesContext()

  if (!messageType) {
    return null
  }

  const messageMap = {
    [MESSAGE_TYPES.ERROR]: {
      title: "Error Type 1",
      data: errorMessages,
    },
    [MESSAGE_TYPES.INFO]: {
      title: "Info Type 3",
      data: infoMessages,
    },
    [MESSAGE_TYPES.WARNING]: {
      title: "Warning Type 2",
      data: warningMessages,
    },
  }
  const messageData = messageMap[messageType]

  return (
    <div className={styles['message-container']}>
      <h1 className={styles.title}>{messageData.title}</h1>
      <p className={styles['sub-title']}>{`Count ${messageData.data.length}`}</p>
      <div className={styles['message-data-list']}>
        {messageData.data.map((message) => (
          <div className={`${styles.notification} ${styles[messageType]}`} key={message.id}>
            <p className={styles.message}>{message.message}</p>
            <div
              className={styles['clear-cta']}
              onClick={onClearMessage(message.id, messageType)}
              role="button"
              tabIndex="0"
            >
              <span>Clear</span>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default Message
