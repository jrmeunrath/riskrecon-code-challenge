import React from "react";

import Button from "../button";
import Message from "./Message";

import { MESSAGE_TYPES } from "./constants";

import { useMessagesContext } from "../../contexts/messages.context";

import styles from "./MessageNotification.module.scss";

function MessageNotification({ isApiStarted, onStartFeed }) {
  const { onResetMessageContext } = useMessagesContext();

  return (
    <div>
      <div className={styles["btn-container"]}>
        <Button
          label={isApiStarted ? "Stop" : "Start"}
          onHandleClick={onStartFeed}
        />
        <Button label={"Clear"} onHandleClick={onResetMessageContext} />
      </div>
      <div className={styles["messages-container"]}>
        <Message messageType={MESSAGE_TYPES.ERROR} />
        <Message messageType={MESSAGE_TYPES.WARNING} />
        <Message messageType={MESSAGE_TYPES.INFO} />
      </div>
    </div>
  );
}

export default MessageNotification;
