export const MESSAGE_TYPES = {
  ERROR: "errorMessages",
  INFO: "infoMessages",
  WARNING: "warningMessages",
}
