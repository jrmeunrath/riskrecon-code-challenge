import React from "react"
import MessageNotificationWidget from "../message-notification-widget"
import Api from "../../api"

import styles from "./MessageList.module.scss"

class MessageList extends React.PureComponent {
  constructor(...args) {
    super(...args)
    this.state = {
      messages: [],
    }
  }

  api = new Api({
    messageCallback: (message) => {
      this.messageCallback(message)
    },
  })

  componentDidMount() {
    this.api.start()
  }

  messageCallback(message) {
    const { messages } = this.state
    this.setState({
      messages: [...messages.slice(), message],
    })
  }

  handleClick = () => {
    const isApiStarted = this.api.isStarted()
    if (isApiStarted) {
      this.api.stop()
    } else {
      this.api.start()
    }
    this.forceUpdate()
  }

  render() {
    const isApiStarted = this.api.isStarted()
    return (
      <div className={styles.container}>
        <MessageNotificationWidget
          isApiStarted={isApiStarted}
          onStartFeed={this.handleClick}
          messages={this.state.messages}
        />
      </div>
    )
  }
}

export default MessageList
