import React from 'react'

import { BTN_THEME } from './constants'
import styles from './Button.module.scss'

function Button({ label, onHandleClick, theme = BTN_THEME.PRIMARY, }) {
  return (
    <button
      className={`${styles.btn} ${styles[theme]}`}
      onClick={onHandleClick}
    >
      {label}
    </button>
  )
}

export default Button
