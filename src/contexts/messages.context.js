import React, { useCallback, useEffect, useState } from "react"
import { createCustomContext } from "./create-context"

const MessagesContext = createCustomContext()

export function useContextSetup(messages) {
  const initialMessageContext = {
    errorMessages: [],
    infoMessages: [],
    warningMessages: [],
  }
  const [messageContext, setMessageContext] = useState(initialMessageContext)
  const onResetMessageContext = useCallback(() => {
    setMessageContext(initialMessageContext)
  }, [initialMessageContext, setMessageContext])
  const onClearMessage = useCallback(
    (id, messageType) => () => {
      setMessageContext((prevState) => {
        return {
          ...prevState,
          [messageType]: messageContext[messageType].filter(
            (message) => message.id !== id
          ),
        }
      })
    },
    [messageContext, setMessageContext]
  )

  useEffect(() => {
    const errorMessages = [
      ...messages.filter((message) => message.priority === 1),
    ]
    const infoMessages = [
      ...messages.filter((message) => message.priority === 3),
    ]
    const warningMessages = [
      ...messages.filter((message) => message.priority === 2),
    ]

    setMessageContext({
      errorMessages: errorMessages.reverse(),
      infoMessages: infoMessages.reverse(),
      warningMessages: warningMessages.reverse(),
    })
  }, [messages])

  return {
    errorMessages: messageContext.errorMessages,
    infoMessages: messageContext.infoMessages,
    onClearMessage,
    onResetMessageContext,
    warningMessages: messageContext.warningMessages,
  }
}

export function MessagesContextProvider({ children, messages }) {
  const value = useContextSetup(messages)

  return (
    <MessagesContext.Provider value={value}>
      {children}
    </MessagesContext.Provider>
  )
}

export const useMessagesContext = MessagesContext.useCustomContext
