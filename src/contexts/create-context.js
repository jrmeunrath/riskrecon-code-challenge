import {
  createContext,
  useContext,
} from 'react'

export function createCustomContext() {
  const context = createContext(undefined)
  function useCustomContext() {
    const contextValue = useContext(context)

    if (!contextValue) {
      console.error('The created useContext must be a child of its Context Provider')
    }
    return contextValue
  }
  return { Provider: context.Provider, useCustomContext }
}
